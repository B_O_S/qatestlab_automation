﻿
Для повышения вероятности корректной работы проекта необходимо:
• сохранить его как можно выше, ближе к коренному каталогу, путь к папке проекта не должен содержать пробелов;
• имя папки должно быть простым, состоять из букв и/или цифр, без пробелов и символов, например «TestsProject1»;
--------------------------------------------------------------------------------------------------------------
Кроме того, для корректной работы IEDriverServer.exe необходимо выполнить следующие предварительные настройки
браузера Internet Explorer:
• установить: Свойства браузера —> вкладка Общие —> Домашняя страница —> кнопка Новая вкладка;
• установить: Свойства браузера —> вкладка Общие —> Автозагрузка —> радиобаттон Начать с домашней страницы;
• установить: Свойства браузера —> вкладка Безопасность —> Уровень безопасности для данной зоны —> Включить
защищенный режим —> чекбокс установить в положение Enable (см. примечание);
• установить: повторить выполнение предыдущего пункта для оставшихся трех зон;
• закрыть все вкладки браузера оставив только одну Новую вкладку.

Примечание: не имеет значения установлен чекбокс в положение Enable or Disable, важно лишь чтобы у всех
четырех зон это положение совпадало.
--------------------------------------------------------------------------------------------------------------
Также в случае некорректной работы IEDriverServer.exe (не находит фактически присутствующие элементы даже по
простым селекторам) необходимо добавить ключ в реестр Windows:
• запускаем редактор реестра regedit.exe и далее следуем инструкции:

For IE 11 only, you will need to set a registry entry on the target computer so that the driver can maintain
a connection to the instance of Internet Explorer it creates.

For 32-bit Windows installations, the key you must examine in the registry editor is
HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BFCACHE.

For 64-bit Windows installations, the key is
HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BFCACHE.

Please note that the FEATURE_BFCACHE subkey may or may not be present, and should be created if it is
not present. Important: Inside this key, create a DWORD value named iexplore.exe with the value of 0.

Is the same as executing a .reg file with this content (example for the windows 64 bits above):

Windows Registry Editor Version 5.00

[HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\Internet Explorer\Main\FeatureControl\FEATURE_BFCACHE]
"iexplore.exe"=dword:00000000
--------------------------------------------------------------------------------------------------------------
Best regards.
Oleg Bura