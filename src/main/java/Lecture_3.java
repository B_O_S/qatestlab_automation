import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

class Lecture_3 {

    public static class AddCategory extends Library implements Runnable {

    public void run() {

/**вход в админпанель*/
            LogInAdminPanel();
            sleep(3000);
/**двигаем курсор к нужному пункту меню*/
            wait.until(ExpectedConditions.elementToBeClickable(By.id("subtab-AdminCatalog")));
            builder.moveToElement(driver.findElement(By.id("subtab-AdminCatalog"))).build().perform();
/**кликаем на пункт сабменю и открываем необходимую страницу*/
            wait.until(ExpectedConditions.elementToBeClickable(By.id("subtab-AdminCategories")));
            efdriver.findElement(By.id("subtab-AdminCategories")).click();
/**кликаем на кнопку добавления категории*/
            wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("#page-header-desc-category-new_category")));
            efdriver.findElement(By.cssSelector("#page-header-desc-category-new_category")).click();
/**вводим имя новой категории, которое формируем с помощью цифрового рендомайзера*/
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@id='name_1']")));
            String newCategoryName = "Category" + rand.nextInt(999);
            efdriver.findElement(By.xpath("//input[@id='name_1']")).sendKeys(newCategoryName);
/**кликаем на кнопку «сохранить»*/
            efdriver.findElement(By.xpath("//button[@id='category_form_submit_btn']")).click();
            wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[@id='content']/div[3]/div")));
/**проверяем содержимое предупреждающего сообщения на успешное сообщение*/
            try {
                String textElem = driver.findElement(By.xpath("//div[@id='content']/div[3]/div")).getText();
                String textElemForAssert = textElem.replace("\n", " ");
                Assert.assertEquals(textElemForAssert, "× Создано");
                System.out.println("\033[32;1mCheck the message about the category creation success - \033[36;1mpassed\033[0m");
            } catch (AssertionError assErrE) {
                System.out.println("\033[32;1mCheck the message about the category creation success - \033[31;1mfailed\033[0m");
                //noinspection ThrowablePrintedToSystemOut
                System.out.println(assErrE);
                Thread.currentThread().interrupt();
                return;
            }
/**вводим имя новой категории в поле фильтра*/
            efdriver.findElement(By.xpath("//table[@id='table-category']/thead/tr[2]/th[3]/input")).sendKeys(newCategoryName);
            efdriver.findElement(By.cssSelector("#submitFilterButtoncategory")).click();
            try {
/**сравниваем количество записей подсчитанное страницей после применения фильтра с искомой единицей*/
                Assert.assertEquals(driver.findElement(By.cssSelector("#form-category > div > div.panel-heading > span.badge")).getText(), "1");
/**сравниваем текст поля отфильтрованной записи содержащего имя категории с именем созданной новой категории*/
                Assert.assertEquals(driver.findElement(By.xpath("//table[@id='table-category']//tr//td[3]")).getText(), newCategoryName);
                System.out.println("\033[32;1mCheck the new created category presence - \033[36;1mpassed\033[0m");
            } catch (Exception Excp) {
/**исключение обрабатывающее отсутствие элемента*/
                System.out.println("\033[32;1mCheck the new created category presence - \033[31;1mfailed\033[0m");
                System.out.println(Excp.toString().substring(0, Excp.toString().indexOf("\n")));
                Thread.currentThread().interrupt();
                return;
            } catch (AssertionError assExcpE) {
/**исключение обрабатывающее ошибку сравнения*/
                System.out.println("\033[32;1mCheck the new created category presence - \033[31;1mfailed\033[0m");
                //noinspection ThrowablePrintedToSystemOut
                System.out.println(assExcpE);
                Thread.currentThread().interrupt();
                return;
            }
/**выход из админпанели*/
            LogOutAdminPanel();
            sleep(2000);
/**закрытие браузера*/
            driver.quit();
        }
    }
}
