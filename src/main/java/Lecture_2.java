import org.openqa.selenium.By;
import org.testng.Assert;

class Lecture_2 {

    public static class LogInAdmPanel extends Library implements Runnable {

        public void run() {
            LogInAdminPanel();
            sleep(5000);

            LogOutAdminPanel();
            sleep(2000);
            driver.quit();
        }

    }

    public static class WorkMainMenu extends Library implements Runnable {

        public void run() {
            LogInAdminPanel();
            sleep(4000);

            String[] MenuItems = {
                    "tab-AdminDashboard",
                    "subtab-AdminParentOrders",
                    "subtab-AdminCatalog",
                    "subtab-AdminParentCustomer",
                    "subtab-AdminParentCustomerThreads",
                    "subtab-AdminStats",
                    "subtab-AdminParentModulesSf",
                    "subtab-AdminParentThemes",
                    "subtab-AdminParentShipping",
                    "subtab-AdminParentPayment",
                    "subtab-AdminInternational",
                    "subtab-ShopParameters",
                    "subtab-AdminAdvancedParameters"
            };
//            for (int itm = 0; itm < MenuItems.length; itm++) {
            for (String MenuItem : MenuItems) {
                efdriver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
                System.out.println("Menu Item: " + driver.findElement(By.id(MenuItem)).getText());
                efdriver.findElement(By.id(MenuItem)).click();
                sleep(1000);
                String CurrentTitle = driver.getTitle();
                System.out.println("Page Title: " + CurrentTitle);
                efdriver.navigate().refresh();
                sleep(1000);
                try {
                    Assert.assertEquals(driver.getTitle(), CurrentTitle);
                    System.out.println("\033[32;1mCheck after refresh - \033[36;1mpassed\033[0m");
                } catch (AssertionError assErrE) {
                    System.out.println("\033[32;1mCheck after refresh - \033[31;1mfailed\033[0m");
                    //noinspection ThrowablePrintedToSystemOut
                    System.out.println(assErrE);
                }
                System.out.println();
            }

            LogOutAdminPanel();
            sleep(2000);
            driver.quit();
        }
    }
}

