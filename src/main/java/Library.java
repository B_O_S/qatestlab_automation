import java.awt.*;
import javax.swing.*;
import java.awt.Dimension;
import java.awt.Point;
import java.util.Arrays;
import java.util.Random;
import java.awt.event.ActionEvent;
import java.util.stream.Collectors;
import java.awt.event.ActionListener;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;

class Library {

    static WebDriver driver;
    static WebDriver efdriver;
    static DialogWindow dw = new DialogWindow("Selection");
    WebDriverWait wait = new WebDriverWait(driver, 10);
    Actions builder = new Actions(driver);
    Random rand = new Random();

    void LogInAdminPanel() {
        efdriver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        efdriver.findElement(By.id("email")).sendKeys("webinar.test@gmail.com");
        efdriver.findElement(By.id("passwd")).sendKeys("Xcg7299bnSmMuRLp9ITw");
        efdriver.findElement(By.name("submitLogin")).click();
    }

    void LogOutAdminPanel() {
        wait.until(ExpectedConditions.elementToBeClickable(By.id("header_employee_box")));
        efdriver.findElement(By.id("header_employee_box")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.id("header_logout")));
        sleep(2000);
        efdriver.findElement(By.id("header_logout")).click();
    }

    static void sleep(int time) {
        try {
            Thread.sleep(time);
        } catch(InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static void DialogWindowToRightBottom() {
        java.awt.Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        java.awt.Dimension size = dw.getSize();
        Insets scnMax = Toolkit.getDefaultToolkit().getScreenInsets(dw.getGraphicsConfiguration());
        int taskBarSize = scnMax.bottom;
        java.awt.Point p = new Point();
        p.setLocation(screenSize.getWidth() - size.getWidth(),
                screenSize.getHeight() - size.getHeight() - taskBarSize);
        dw.setLocation(p);
    }

    public static class DialogWindow extends JFrame {

        JButton b1, b2;
        JRadioButtonMenuItem rbmi1, rbmi2, rbmi3, rbmi4, rbmi5, rbmi6;
        JCheckBoxMenuItem cbmi1;
        JPanel panel1 = new JPanel();
        JPanel panel2 = new JPanel();
        JPanel panel3 = new JPanel();
        ButtonGroup rbmigroup1 = new ButtonGroup();
        ButtonGroup rbmigroup2 = new ButtonGroup();
        eHandler handler = new eHandler();

        DialogWindow(String s) {
            super(s);
            setLayout(null);
            b1 = new JButton("Start");
            b1.setBounds(20, 180, 100, 30);
            b2 = new JButton("Cancel");
            b2.setBounds(140, 180, 100, 30);
            rbmi1 = new JRadioButtonMenuItem("Chrome");
            rbmi1.setBounds(20, 22, 75, 20);
            rbmi2 = new JRadioButtonMenuItem("FireFox");
            rbmi2.setBounds(100, 22, 75, 20);
            rbmi3 = new JRadioButtonMenuItem("IExplorer");
            rbmi3.setBounds(170, 22, 75, 20);
            rbmi4 = new JRadioButtonMenuItem("Log in/out Admin Panel");
            rbmi4.setBounds(20, 72, 200, 20);
            rbmi5 = new JRadioButtonMenuItem("Check the main menu works");
            rbmi5.setBounds(20, 94, 200, 20);
            rbmi6 = new JRadioButtonMenuItem("Add Category");
            rbmi6.setBounds(20, 142, 100, 20);
            cbmi1 = new JCheckBoxMenuItem("Log on/off");
            cbmi1.setBounds(160, 142, 80, 20);
            panel1.setBorder(BorderFactory
                    .createTitledBorder(BorderFactory.createEtchedBorder(), "Browser Selection"));
            panel1.setBounds(6, 3, 250, 50);
            panel2.setBorder(BorderFactory
                    .createTitledBorder(BorderFactory.createEtchedBorder(), "Tests to the Lecture #2"));
            panel2.setBounds(6, 53, 250, 70);
            panel3.setBorder(BorderFactory
                    .createTitledBorder(BorderFactory.createEtchedBorder(), "Test to the Lecture #3"));
            panel3.setBounds(6, 123, 250, 50);
            rbmigroup1.add(rbmi1);
            rbmigroup1.add(rbmi2);
            rbmigroup1.add(rbmi3);
            rbmigroup2.add(rbmi4);
            rbmigroup2.add(rbmi5);
            rbmigroup2.add(rbmi6);
            add(rbmi1);
            add(rbmi2);
            add(rbmi3);
            add(rbmi4);
            add(rbmi5);
            add(rbmi6);
            add(cbmi1);
            add(panel1);
            add(panel2);
            add(panel3);
            add(b1);
            add(b2);
            b1.addActionListener(handler);
            b2.addActionListener(handler);
        }

        public class eHandler implements ActionListener {

            public void actionPerformed(ActionEvent actEvE) {
                if (actEvE.getSource() == b1) {

                    if (rbmi1.isSelected() && (rbmi4.isSelected() || rbmi5.isSelected() || rbmi6.isSelected()) && cbmi1.isSelected()) {
                        System.setProperty("webdriver.chrome.driver", Library.class
                                .getResource("/chromedriver.exe").getPath());
                        driver = new ChromeDriver();
                        driver.manage().window().setSize(new org.openqa.selenium.Dimension(1100,750));
                        driver.manage().window().setPosition(new org.openqa.selenium.Point(0,0));
                        efdriver = new EventFiringWebDriver(driver).register(new MyWebDriverEventListener());
                    }

                    if (rbmi1.isSelected() && (rbmi4.isSelected() || rbmi5.isSelected() || rbmi6.isSelected()) && !cbmi1.isSelected()) {
                        System.setProperty("webdriver.chrome.driver", Library.class
                                .getResource("/chromedriver.exe").getPath());
                        driver = new ChromeDriver();
                        driver.manage().window().setSize(new org.openqa.selenium.Dimension(1100,750));
                        driver.manage().window().setPosition(new org.openqa.selenium.Point(0,0));
                        efdriver = driver;
                    }

                    if (rbmi2.isSelected() && (rbmi4.isSelected() || rbmi5.isSelected() || rbmi6.isSelected()) && cbmi1.isSelected()) {
                        System.setProperty("webdriver.gecko.driver", Library.class
                                .getResource("/geckodriver.exe").getPath());
                        System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
                        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
                        driver = new FirefoxDriver();
                        driver.manage().window().setSize(new org.openqa.selenium.Dimension(1100,750));
                        driver.manage().window().setPosition(new org.openqa.selenium.Point(0,0));
                        efdriver = new EventFiringWebDriver(driver).register(new MyWebDriverEventListener());
                    }

                    if (rbmi2.isSelected() && (rbmi4.isSelected() || rbmi5.isSelected() || rbmi6.isSelected()) && !cbmi1.isSelected()) {
                        System.setProperty("webdriver.gecko.driver", Library.class
                              .getResource("/geckodriver.exe").getPath());
                        System.setProperty(FirefoxDriver.SystemProperty.DRIVER_USE_MARIONETTE,"true");
                        System.setProperty(FirefoxDriver.SystemProperty.BROWSER_LOGFILE,"/dev/null");
                        driver = new FirefoxDriver();
                        driver.manage().window().setSize(new org.openqa.selenium.Dimension(1100,750));
                        driver.manage().window().setPosition(new org.openqa.selenium.Point(0,0));
                        efdriver = driver;
                    }

                    /*InternetExplorerOptions options = new InternetExplorerOptions();
                    options.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
                    options.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
                    options.setCapability("requireWindowFocus", true);*/

                    if (rbmi3.isSelected() && (rbmi4.isSelected() || rbmi5.isSelected() || rbmi6.isSelected()) && cbmi1.isSelected()) {
                        System.setProperty("webdriver.ie.driver", Library.class
                                .getResource("/IEDriverServer.exe").getPath());
                        try {
                            driver = new InternetExplorerDriver();
                            driver.manage().window().setSize(new org.openqa.selenium.Dimension(1100,750));
                            driver.manage().window().setPosition(new org.openqa.selenium.Point(0,0));
                            efdriver = new EventFiringWebDriver(driver).register(new MyWebDriverEventListener());
                        } catch (SessionNotCreatedException sce) {
                            driver = null;
                            JOptionPane.showMessageDialog(dw,
                                    "Protected Mode settings are not the same for all zones. Enable\n" +
                                            "Protected Mode must be set to the same value (enabled or disabled)\n" +
                                            "for all zones in the Internet Explorer—>Settings—>Security.");
                        }
                    }

                    if (rbmi3.isSelected() && (rbmi4.isSelected() || rbmi5.isSelected() || rbmi6.isSelected()) && !cbmi1.isSelected()) {
                        System.setProperty("webdriver.ie.driver", Library.class
                                .getResource("/IEDriverServer.exe").getPath());
                        try {
                            driver = new InternetExplorerDriver();
                            driver.manage().window().setSize(new org.openqa.selenium.Dimension(1100,750));
                            driver.manage().window().setPosition(new org.openqa.selenium.Point(0,0));
                            efdriver = driver;
                        } catch (SessionNotCreatedException sce) {
                            driver = null;
                            JOptionPane.showMessageDialog(dw,
                                    "Protected Mode settings are not the same for all zones. Enable\n" +
                                            "Protected Mode must be set to the same value (enabled or disabled)\n" +
                                            "for all zones in the Internet Explorer—>Settings—>Security.");
                        }
                    }

                    if (rbmi4.isSelected() && (rbmi1.isSelected() || rbmi2.isSelected() || rbmi3.isSelected()) && driver != null) {
                        DialogWindowToRightBottom();
                        sleep(1000);
                        new Thread(new Lecture_2.LogInAdmPanel()).start();
                    }

                    if (rbmi5.isSelected() && (rbmi1.isSelected() || rbmi2.isSelected() || rbmi3.isSelected()) && driver != null) {
                        DialogWindowToRightBottom();
                        sleep(1000);
                        new Thread(new Lecture_2.WorkMainMenu()).start();
                    }

                    if (rbmi6.isSelected() && (rbmi1.isSelected() || rbmi2.isSelected() || rbmi3.isSelected()) && driver != null) {
                        DialogWindowToRightBottom();
                        sleep(1000);
                        new Thread(new Lecture_3.AddCategory()).start();
                    }

                    if (!rbmi1.isSelected() && !rbmi2.isSelected() && !rbmi3.isSelected()) {
                        JOptionPane.showMessageDialog(dw, "Select browser to run please");
                    }

                    if (!rbmi4.isSelected() && !rbmi5.isSelected() && !rbmi6.isSelected()) {
                        JOptionPane.showMessageDialog(dw, "Select test to run please");
                    }
                }

                if (actEvE.getSource() == b2) {
                    System.exit(0);
                }
            }
        }
    }

    public static class MyWebDriverEventListener implements WebDriverEventListener {

        private String elementDescription(WebElement element) {
            String description = "\033[33;1mtag: \"" + element.getTagName() + "\"\033[0m";
            if (element.getAttribute("id") != null) {
                description += " | \033[94;1mid: \"" + element.getAttribute("id") + "\"\033[0m";
            }
            if (element.getAttribute("name") != null) {
                description += " | \033[95;1mname: \"" + element.getAttribute("name") + "\"\033[0m";
            }
            if (!element.getText().equals("")) {
                description += " | \033[96;1mtext: \"" + element.getText() + "\"\033[0m";
            }
            return description;
        }

        public void beforeFindBy(By by, WebElement element, WebDriver efdriver) {}
        public void afterFindBy(By by, WebElement element, WebDriver efdriver) {
            System.out.println("WebDriver found element - " + elementDescription(driver.findElement(by)));
        }
        public void beforeClickOn(WebElement element, WebDriver efdriver) {
            System.out.println("WebDriver click on element - " + elementDescription(element));
        }
        public void afterClickOn(WebElement element, WebDriver efdriver) {}
        public void beforeChangeValueOf(WebElement element, WebDriver efdriver, CharSequence[] keysToSend) {
            if (keysToSend != null) {
                String value = Arrays.stream(keysToSend).map(CharSequence::toString).collect(Collectors.joining());
                System.out.println(String.format("WebDriver will be changed value of element %s to: %s", elementDescription(element), value));
            } else {
                System.out.println(String.format("WebDriver will be cleared element %s", elementDescription(element)));
            }
        }
        public void afterChangeValueOf(WebElement element, WebDriver efdriver, CharSequence[] charSequences) {
            System.out.println("WebDriver changed value for element - " + elementDescription(element));
        }
        public void beforeNavigateTo(String url, WebDriver efdriver) {}
        public void afterNavigateTo(String url, WebDriver efdriver) {
            System.out.println("WebDriver navigated to \"" + url + "\"");
        }
        public void beforeNavigateBack(WebDriver efdriver) {}
        public void afterNavigateBack(WebDriver efdriver) {}
        public void beforeNavigateForward(WebDriver efdriver) {}
        public void afterNavigateForward(WebDriver efdriver) {}
        public void beforeNavigateRefresh(WebDriver efdriver) {}
        public void afterNavigateRefresh(WebDriver efdriver) {
            System.out.println("WebDriver reloaded the page");
        }
        public void beforeScript(String script, WebDriver efdriver) {}
        public void afterScript(String script, WebDriver efdriver) {}
        public void beforeGetText(WebElement element, WebDriver efdriver) {}
        public void afterGetText(WebElement element, WebDriver efdriver, String text) {}
        public void beforeSwitchToWindow(String script, WebDriver efdriver) {}
        public void afterSwitchToWindow(String script, WebDriver efdriver) {}
        public <X> void beforeGetScreenshotAs(OutputType<X> target) {}
        public <X> void afterGetScreenshotAs(OutputType<X> target, X screenshot) {}
        public void beforeAlertAccept(WebDriver efdriver) {}
        public void afterAlertAccept(WebDriver efdriver) {}
        public void beforeAlertDismiss(WebDriver efdriver) {}
        public void afterAlertDismiss(WebDriver efdriver) {}
        public void onException(Throwable error, WebDriver efdriver) {}
    }
}
