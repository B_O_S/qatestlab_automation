import javax.swing.*;

public class Start extends Library {

    public static void main(String args[]) {

        dw.setVisible(true);
        dw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        dw.setSize(268, 250);
        dw.setResizable(false);
        dw.setLocationRelativeTo(null);
        dw.setAlwaysOnTop(true);
    }

}

